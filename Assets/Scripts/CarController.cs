using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
	private Vector3 startPosition, startRotation;

	[Range(-1f, 1f)]
	public float accel, turning;

	[SerializeField] float timeSinceStart;

	[Header("Fitness")]
	[SerializeField] float overallFitness;
	[SerializeField] float distanceMultiplier = 1.4f;
	[SerializeField] float avgSpeedMultiplier = 0.2f;
	[SerializeField] float sensorMultiplier = 0.1f; // defines the importance of being in the center of the track

	public event Action OnCarCrash;

	private Vector3 carPosition;

	public bool HasCrashed = false;

	private void Awake()
	{
		startPosition = transform.position;
		startRotation = transform.eulerAngles;
	}

	public void ResetWithNetwork(NeuralNet net)
	{
		//Reset();
	}

	public void ResetCar()
	{
		//totalDistanceTraveled = 0f;
		timeSinceStart = 0f;
		//avgSpeed = 0f;
		//lastPosition = startPosition;

		overallFitness = 0;
		
		transform.position = startPosition;
		
		transform.eulerAngles = startRotation;
	}

	private void FixedUpdate()
	{
		//UpdateSensors();

		//lastPosition = transform.position;

		//(accel, turning) = network.RunNetwork(rightDistance, leftDistance, frontDistance);

		//MoveCar(accel, turning);

		//timeSinceStart += Time.deltaTime;

		//CalculateFitness();
	}

	private void Death()
	{
		//FindObjectOfType<GeneticManager>().Death(overallFitness, network);
	}

	private void CalculateFitness()
	{
		//totalDistanceTraveled += Vector3.Distance(transform.position, lastPosition);
		//avgSpeed = totalDistanceTraveled / timeSinceStart;

		//float avgDistanceToWall = (frontDistance + leftDistance + rightDistance) / 3;
		//overallFitness = (totalDistanceTraveled * distanceMultiplier) + (avgSpeed * avgSpeedMultiplier) + (avgDistanceToWall * sensorMultiplier);

		//if (timeSinceStart > 20 && overallFitness < 40)
		//{
		//	Death();
		//	return;
		//}

		//if(overallFitness >= 1000)
		//{
		//	//TODO: save to json
		//	Death();
		//}
	}

	public void MoveCar(float acceleration, float rotation)
	{
		// 11.4 got through trial and error to control acceleration
		carPosition = Vector3.Lerp(Vector3.zero, new(0, 0, acceleration * 11.4f), 0.02f);
		carPosition = transform.TransformDirection(carPosition);

		transform.position += carPosition;

		//0.02 to smooth the rotation, as in Lerp
		transform.eulerAngles += new Vector3(0, 90 * rotation * 0.02f, 0);
	}

	public (float, float, float) GetSensors()
	{
		Vector3 right = transform.forward + transform.right;
		Vector3 left = transform.forward - transform.right;
		Vector3 forward = transform.forward;

		RaycastHit hit;

		float rightDistance = 0.0f;
		float leftDistance  = 0.0f;
		float frontDistance = 0.0f;

		if(Physics.Raycast(transform.position, right, out hit))
		{
			rightDistance = hit.distance;

			Debug.DrawLine(transform.position, hit.point, Color.red);
		}

		if (Physics.Raycast(transform.position, left, out hit))
		{
			leftDistance = hit.distance;

			Debug.DrawLine(transform.position, hit.point, Color.red);
		}

		if (Physics.Raycast(transform.position, forward, out hit))
		{
			frontDistance = hit.distance;

			Debug.DrawLine(transform.position, hit.point, Color.red);
		}

		return (rightDistance, leftDistance, frontDistance);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if(OnCarCrash is not null) { OnCarCrash(); }
		HasCrashed = true;
	}
}
