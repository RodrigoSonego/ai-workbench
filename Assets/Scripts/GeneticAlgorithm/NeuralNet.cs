using System.Collections.Generic;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra;

public class NeuralNet
{
	public int numberOfInputs = 3;
	public int numberOfOutputs = 2;

	public Matrix<float> inputLayer;
	public Matrix<float> outputLayer;

	public List<Matrix<float>> hiddenLayers = new List<Matrix<float>>();

	public List<Matrix<float>> weights = new List<Matrix<float>>();
	
	public List<float> biases = new List<float>();

	public float fitness;

	private int layerCount, neuronCount;

	public NeuralNet(int hiddenLayerCount, int hiddenNeuronCount, int numberOfInputs, int numberOfOutputs)
	{
		layerCount = hiddenLayerCount;
		neuronCount = hiddenNeuronCount;

		this.numberOfInputs = numberOfInputs;
		this.numberOfOutputs = numberOfOutputs;

		inputLayer = Matrix<float>.Build.Dense(1, numberOfInputs);
		outputLayer = Matrix<float>.Build.Dense(1, numberOfOutputs);
	}

	// TODO: take parameters out
	public void Initialise(int hiddenLayerCount, int hiddenNeuronCount)
	{
		weights.Clear();
		biases.Clear();

		InitialiseHiddenLayers(hiddenLayerCount, hiddenNeuronCount);

		for (int i = 0; i < hiddenLayerCount; ++i) 
		{
			biases.Add(Random.Range(-1f, 1f));

			//Weight handling
			if ( i == 0 ) // Weight from input layer to first hidden layer
			{
				Matrix<float> inputToH1 = Matrix<float>.Build.Dense(numberOfInputs, hiddenNeuronCount);
				weights.Add(inputToH1);
			}
		}

		Matrix<float> outputWeight = Matrix<float>.Build.Dense(hiddenNeuronCount, numberOfOutputs);
		weights.Add(outputWeight);
		biases.Add(Random.Range(-1f, 1f));

		RandomiseWeights();
	}

	public NeuralNet InitaliseCopy(int hiddenLayerCount, int hiddenNeuronCount)
	{
		NeuralNet net = new NeuralNet(hiddenLayerCount, hiddenNeuronCount, numberOfInputs, numberOfOutputs);

		net.weights = new List<Matrix<float>>(weights);
		net.biases = new List<float>(biases);

		net.InitialiseHiddenLayers(hiddenLayerCount, hiddenNeuronCount);

		return net;
	}

	// TODO: take parameters out
	private void InitialiseHiddenLayers(int hiddenLayerCount, int hiddenNeuronCount)
	{
		inputLayer.Clear();
		hiddenLayers.Clear();
		outputLayer.Clear();

		for (int i = 0; i < hiddenLayerCount; ++i)
		{
			Matrix<float> layer = Matrix<float>.Build.Dense(1, hiddenNeuronCount);

			hiddenLayers.Add(layer);
		}
	}

	private void RandomiseWeights()
	{
		for (int w = 0; w < weights.Count; ++w)
		{
			for (int row = 0; row < weights[w].RowCount; ++row)
			{
				for (int col = 0; col < weights[w].ColumnCount; ++col)
				{
					weights[w][row, col] = Random.Range(-1f, 1f);
				}
			}
		}
	}

	public (float, float) RunNetwork (float input0, float input1, float input2)
	{
		inputLayer[0, 0] = input0;
		inputLayer[0, 1] = input1;
		inputLayer[0, 2] = input2;

		inputLayer = inputLayer.PointwiseTanh(); // PointwiseTanh keeps the value between -1 and 1

		hiddenLayers[0] = ((inputLayer * weights[0]) + biases[0]).PointwiseTanh();

		for(int i = 1; i< hiddenLayers.Count; ++i)

		{
			hiddenLayers[i] = ( (hiddenLayers[i-1] * weights[i]) + biases[i] ).PointwiseTanh();
		}

		outputLayer = ((hiddenLayers[hiddenLayers.Count - 1] * weights[weights.Count - 1]) + biases[biases.Count - 1]).PointwiseTanh();

		//First output is acceleration and second output is steering
		return (Sigmoid(outputLayer[0,0]), (float)System.Math.Tanh(outputLayer[0,1]));
	}


	private float Sigmoid(float s)
	{
		return 1 / (1 + Mathf.Exp(-s));
	}
}
