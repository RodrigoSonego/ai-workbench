using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MathNet.Numerics.LinearAlgebra;
using System.Linq;

public class GeneticManager : MonoBehaviour
{
    [SerializeField] CarController carController;

    [Header("Controls")]
    [SerializeField] int initialPopulation = 85;
    [Range(0f, 1f)]
    [SerializeField] float mutationRate = 0.055f;

    [SerializeField] int numberOfInputs = 3;
    [SerializeField] int numberOfOutputs = 2;

    [Header("Crossover Controls")]
    [SerializeField] int bestAgentSelection = 8;
    [SerializeField] int worstAgentSelection = 3;
    [SerializeField] int numberToCrossover;

    private List<int> genePool = new List<int>();

    private int naturallySelectedCount;

    private NeuralNet[] population;

    [Header("Debug View")]
    [SerializeField] int currentGeneration;
	[SerializeField] int currentGenome;

    [SerializeField] float biggestFitness;

	private void Start()
	{
        CreatePopulation();
	}

    private void CreatePopulation()
    {
        population = new NeuralNet[initialPopulation];
        FillPopulationWithRandomValues(population, 0);
        ResetToCurrentGenome();
    }
	private void ResetToCurrentGenome()
	{
        carController.ResetWithNetwork(population[currentGenome]);
	}

	private void FillPopulationWithRandomValues(NeuralNet[] population, int startIndex)
    {
        //while ( startIndex < initialPopulation )
        //{
        //    population[startIndex] = new NeuralNet(carController.layers, carController.neurons, numberOfInputs, numberOfOutputs);
        //    population[startIndex].Initialise(carController.layers, carController.neurons);

        //    ++startIndex;
        //}
    }

    public void Death(float fitness, NeuralNet net)
    {
        if(fitness > biggestFitness) { biggestFitness = fitness; }

        if (currentGenome < population.Length - 1)
        {
            population[currentGenome].fitness = fitness;
            currentGenome++;
            ResetToCurrentGenome();
        }
        else
        {
            Repopulate();
        }
    }

    private void Repopulate()
    {
        print("generation " + currentGeneration + ": biggest fitness= " + biggestFitness);

        genePool.Clear();
        currentGeneration++;
        naturallySelectedCount = 0;
        biggestFitness = 0;


        SortPopulation();

        NeuralNet[] newPopulation = PickBestPopulation();

        Crossover(newPopulation);
		Mutate(newPopulation);

        FillPopulationWithRandomValues(newPopulation, naturallySelectedCount);

        population = newPopulation;

        currentGenome = 0;

        ResetToCurrentGenome();
    }
	private void Crossover(NeuralNet[] newPopulation)
	{
        for (int i = 0; i < numberToCrossover; i+=2)
        {
            int AIndex = i;
            int BIndex = i+1;

            if (genePool.Count >= 2)
            {
                for (int j = 0; j < 100; j++)
                {
                    AIndex = genePool[Random.Range(0, genePool.Count)];
					BIndex = genePool[Random.Range(0, genePool.Count)];
                    
                    if(AIndex != BIndex) { break; }
				}
            }

   //         NeuralNet child1 = new NeuralNet(carController.layers, carController.neurons, numberOfInputs, numberOfOutputs);
			//NeuralNet child2 = new NeuralNet(carController.layers, carController.neurons, numberOfInputs, numberOfOutputs);

   //         child1.Initialise(carController.layers, carController.neurons);
			//child2.Initialise(carController.layers, carController.neurons);

            //child1.fitness = 0;
            //child2.fitness = 0;

            // Crossover Weights
            //for (int w = 0; w < child1.weights.Count; w++)
    //        {
    //            if(Random.Range(0f, 1f) < 0.5f)
    //            {
    //                child1.weights[w] = population[AIndex].weights[w];
				//	child2.weights[w] = population[BIndex].weights[w];
				//}
    //            else
    //            {
				//	child1.weights[w] = population[BIndex].weights[w];
				//	child2.weights[w] = population[AIndex].weights[w];
				//}
    //        }

            // Crossover Biases
			//for (int w = 0; w < child1.biases.Count; w++)
			//{
			//	if (Random.Range(0f, 1f) < 0.5f)
			//	{
			//		child1.biases[w] = population[AIndex].biases[w];
			//		child2.biases[w] = population[BIndex].biases[w];
			//	}
			//	else
			//	{
			//		child1.biases[w] = population[BIndex].biases[w];
			//		child2.biases[w] = population[AIndex].biases[w];
			//	}
			//}

   //         newPopulation[naturallySelectedCount] = child1;

   //         newPopulation[naturallySelectedCount + 1] = child2;

   //         naturallySelectedCount += 2;
		}
	}

	private void Mutate(NeuralNet[] newPopulation)
	{
        for (int i = 0; i < naturallySelectedCount; i++)
        {
            for (int w = 0; w < newPopulation[i].weights.Count; ++w)
            {
                if (Random.Range(0f, 1f) < mutationRate)
                {
                    newPopulation[i].weights[w] = MutateMatrix(newPopulation[i].weights[w]);}
            }
        }
	}

    private Matrix<float> MutateMatrix(Matrix<float> matrix)
    {
        int randomPoints = Random.Range(1, (matrix.RowCount * matrix.ColumnCount) / 7); // dividing by 7 in order ot only pick a few values

        Matrix<float> result = matrix;

        for (int i = 0; i < randomPoints; i++)
        {
            int randomColumn = Random.Range(0, result.ColumnCount);
			int randomRow = Random.Range(0, result.RowCount);

            float mutatedValue = result[randomRow, randomColumn] + Random.Range(-1f, 1f);
			result[randomRow, randomColumn] = Mathf.Clamp(mutatedValue, -1f, 1f);
		}

        return result;
    }

	private void SortPopulation()
    {
		for (int i = 0; i < population.Length; i++)
		{
			for (int j = i; j < population.Length; j++)
			{
				if (population[i].fitness < population[j].fitness)
				{
					NeuralNet temp = population[i];
					population[i] = population[j];
					population[j] = temp;
				}
			}
		}
	}

    private NeuralNet[] PickBestPopulation()
    {
        NeuralNet[] newPopulation = new NeuralNet[initialPopulation];

        for (int i = 0; i < bestAgentSelection; ++i)
        {
            //newPopulation[naturallySelectedCount] = population[i].InitaliseCopy(carController.layers, carController.neurons);
            newPopulation[naturallySelectedCount].fitness = 0;

			naturallySelectedCount++;

            int geneFrequency = Mathf.RoundToInt(population[i].fitness * 10);

            for (int j = 0; j < geneFrequency; j++)
            {
                genePool.Add(i);
            }
        }

        return newPopulation;
    }
}
