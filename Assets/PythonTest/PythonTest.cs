using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Python.Runtime;

public class PythonTest : MonoBehaviour
{
	int count;

	void Update()
	{
		print("update: " +  count);

		using (Py.GIL())
		{
			using dynamic script = Py.Import("test");

			var result = (int)script.add(0, count);

			Debug.Log("vindo do paito: " + count++);
		}

	}
}
