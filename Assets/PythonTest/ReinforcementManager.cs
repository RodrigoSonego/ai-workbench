using System.Collections;
using UnityEngine;
using Python.Runtime;
using Microsoft.CSharp.RuntimeBinder;
using UnityEditor;

public class ReinforcementManager : MonoBehaviour
{
	[SerializeField] CarController car;

	[Header("Model Config")]
	[SerializeField] int numberOfInputs;
	[SerializeField] int numberOfOutputs;
	[SerializeField] float epsilon;
	[SerializeField] float epsilonDecay;
	[SerializeField] float discountFactor;
	[SerializeField] int framesBetweeenLearning = 15;

	[Space]
	[SerializeField] bool willTrain;
	[SerializeField] bool loadSavedModel;

	private PyObject model;

	private Vector3 carPosition;
	private Vector3 carLastPosition;

	private float frontDistance, leftDistance, rightDistance;

	private int numberOfEpisodes = 0;

	dynamic reinforcement;

	int frameCount = 0;

	private float totalReward;

	private float[,] input, new_input;

	private void Start()
	{
		BuildModel();

		car.OnCarCrash += ResetCar;

		reinforcement = Py.Import("reinforce");
	}

	private void FixedUpdate()
	{
		carLastPosition = car.transform.position;

		UpdateDistances();

		input = NormalizeDistances();

		float[] activations = GetAccelAndTurning();

		if (activations is null) { return; }

		float accel = activations[0];
		float turning = activations[1];

		car.MoveCar(accel, turning);

		UpdateDistances();
		new_input = NormalizeDistances();

		carPosition = car.transform.position;

		float reward = CalculateReward(accel);
		totalReward += reward;

		if (willTrain)
		{
			TrainAtIntervals(reward);
		}
	}

	private void BuildModel()
	{
		using (Py.GIL())
		{
			using dynamic reinforcement = Py.Import("reinforce");
			model = reinforcement.build_model(numberOfInputs, numberOfOutputs, loadSavedModel);
		}
	}

	private void ResetCar()
	{
		car.ResetCar();
		numberOfEpisodes++;

		carLastPosition = car.transform.position;

		print($"episode: {numberOfEpisodes}, total reward= {totalReward}");
		epsilon *= epsilonDecay;

		totalReward = 0;

		if (willTrain)
		{
			Learn(-2);
		}
	}

	private float[,] NormalizeDistances()
	{
		var normRight = rightDistance / 50f;
		var normLeft = leftDistance / 50f;
		var normFront = frontDistance / 50f;

		return new float[1, 3] { { normRight, normLeft, normFront } };
	}

	private void Learn(float reward)
	{
		model = reinforcement.learn(reward, new_input, input, discountFactor, model);
	}

	private float CalculateReward(float accel)
	{
		float reward;

		if (accel < 0)
		{
			reward = -0.5f;
		}
		else
		{
			//reward = 0.5f + (((frontDistance + leftDistance + rightDistance) / 3) / 50);
			reward = 0.5f;
		}

		return reward;
	}

	private void UpdateDistances()
	{
		(rightDistance, leftDistance, frontDistance) = car.GetSensors();
	}

	private float[] GetAccelAndTurning()
	{
		try
		{
			return (float[])reinforcement.choose_action(input, epsilon, model);
		}
		catch (RuntimeBinderException _)
		{
			return null;
		}
	}

	public void SaveModel()
	{		
		reinforcement.save_model(model);
	}

	private void TrainAtIntervals(float reward)
	{
		if (frameCount == framesBetweeenLearning)
		{
			Learn(reward);
			frameCount = 0;
		}

		frameCount++;
	}
}